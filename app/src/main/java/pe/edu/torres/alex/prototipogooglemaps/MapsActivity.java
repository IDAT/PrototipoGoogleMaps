package pe.edu.torres.alex.prototipogooglemaps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;

import java.util.ArrayList;
import java.util.Locale;

public class MapsActivity extends FragmentActivity
        implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnInfoWindowClickListener  {

    private static final int COLOR_ORANGE_ARGB = 0xffF57F17;
    private static final int POLYGON_STROKE_WIDTH_PX = 8;
    private static final int LOCATION_REQUEST_CODE = 1;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // TODO - VALORES
        double dblCasaLatitud = -12.033499272501066;
        double dblCasaLongitud = -77.05188796065215;

        //TODO - COORDENADAS
        LatLng llCasa1 =new LatLng(dblCasaLatitud, dblCasaLongitud);
        ArrayList<Mark> aryCoordenadas = new ArrayList<>();
        aryCoordenadas.add(new Mark("Mollendo","Ca. Miramar",new LatLng(-17.023988852534682,-72.00909998474879)));
        aryCoordenadas.add(new Mark("San Martin - Macara","Jr. Macara 163",llCasa1));
        aryCoordenadas.add(new Mark("Jesus Maria","Ca. Jose Carlos Mariategui",new LatLng(-12.076392755416999,-77.04175982644756)));
        aryCoordenadas.add(new Mark("San Martin - Piedritas","Jr. Piedritas",new LatLng(-12.032891875067824,-77.05310557058056)));
        aryCoordenadas.add(new Mark("Villa El Salvador","Sector 2 Grupo 10",new LatLng(-12.20981669494639,-76.93508226777692)));

        for(Mark coordenada : aryCoordenadas) {
            // Marker
            MarkerOptions markerCasa = new MarkerOptions().position(coordenada.latLng)
                    .title(coordenada.title)
                    .draggable(true)
                    //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                    .snippet(coordenada.snippet);

            mMap.addMarker(markerCasa);
        }

        Polyline polyline1 = googleMap.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(
                        new LatLng(-17.023988852534682,-72.00909998474879),
                        llCasa1,
                        new LatLng(-12.076392755416999,-77.04175982644756),
                        new LatLng(-12.032891875067824,-77.05310557058056),
                        new LatLng(-12.20981669494639,-76.93508226777692)));

        polyline1.setEndCap(new RoundCap());
        polyline1.setWidth(POLYGON_STROKE_WIDTH_PX);
        polyline1.setColor(COLOR_ORANGE_ARGB);
        polyline1.setJointType(JointType.ROUND);


        CameraPosition cameraPosition = new CameraPosition.Builder().target(llCasa1).zoom(7).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(llCasa));

        //TODO - CONTROLES
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        //TODO - CONTROL CON PERMISO
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Mostrar diálogo explicativo
            } else {
                // Solicitar permiso
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION_REQUEST_CODE);
            }
        }

        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);

        mMap.getUiSettings().setAllGesturesEnabled(false); //Desactivacion para que pueda funcionar InfoWindowClick
        //TODO - EVENTOS
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMarkerDragListener(this);
        googleMap.setOnInfoWindowClickListener(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            // ¿Permisos asignados?
            if (permissions.length > 0 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            } else {
                Toast.makeText(this, "Error de permisos", Toast.LENGTH_LONG).show();
            }

        }
    }


    @Override
    public void onMarkerDragStart(Marker marker) {
            Toast.makeText(this, "START", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDrag(Marker marker) {
            String newTitle = String.format(Locale.getDefault(),
                    getString(R.string.marker_detail_latlng),
                    marker.getPosition().latitude,
                    marker.getPosition().longitude);

            setTitle(newTitle);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
            Toast.makeText(this, "END", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
            MollendoDialogFragment.newInstance(marker.getTitle(),
                    getString(R.string.mollendo_full_snippet))
                    .show(getSupportFragmentManager(), null);

    }


}
